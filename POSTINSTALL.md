# Configuration

Edit key behaviour and attributes in `/app/data/config.js`

In particular :
- change the default login for admin/password and admin2/password
- set whether edits need login
- set whether reading needs login
- change the site title (optional)
- change the meta values (optional)  

--> Then RESTART the app <--

Best advised to leave path configurations set in `config.js` as they are, pending further testing.

# Images

These need to be uploaded in File Manager (or Terminal) to :
`/app/data/node_modules/@raneto/theme-default/dist/public/images`

Then in your content pages, you can display them with basic Markdown like this, PROVIDING you have not changed paths in `config.js`:
`![pretty image](/images/background.jpg)`

# Help / Content

The app loads a default set of pages which effectively provide help on usage.

But you probably want to replace this with your content, so you will lose the help content.
There is no workaround for this at the moment.
But can always go to https://docs.raneto.com and read the default content there.

