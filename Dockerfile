FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/data /tmp/content /tmp/node_modules

WORKDIR /app/code

RUN curl -sL https://github.com/ryanlelek/Raneto/archive/refs/tags/0.17.5.tar.gz | tar -C /app/code --strip-components 1 -xvzf -

RUN npm install --omit=dev 

RUN mv content/* /tmp/content \
    && mv config/config.js /tmp/config.js \
    && cp -r node_modules/* /tmp/node_modules \
    && rm -R test deprecated logo bin config content node_modules \
    && rm Dockerfile HISTORY.md Makefile .gitignore .dockerignore \
    && ln -s /app/data/config config \
    && ln -s /app/data/content content \
    && ln -s /app/data/node_modules node_modules

ADD start.sh POSTINSTALL.md /app/code/

RUN chmod +x start.sh

EXPOSE 3000
ENV HOST 0.0.0.0
ENV PORT 3000

# CMD ["npm", "start"]
CMD [ "/app/code/start.sh" ]
