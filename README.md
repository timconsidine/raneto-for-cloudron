# Raneto for Cloudron

This repo is cloned from the original and modified to be installed as a custom app on Cloudron.

## CAUTION :
There is an issue (see Github issues) about rendering of markdown code segements when is saved then edited again.
 
**PLEASE SEE ORIGINAL README IN REPO BELOW FOR APP DETAILS.**

https://github.com/ryanlelek/Raneto

All work acknowledged/credited.  No functional changes made to app.  

**PLEASE READ POSTINSTALL.md.**

Just packaged for Cloudron, which has involved supporting separation of /app/code (read only) and /app/data (writable).

## building for cloudron

* download / clone repo to a folder on your local machine
* First Time app packagers :
    * ensure you have cloudron cli installed AND are logged in
    * ensure you have Docker installed
    * ensure you have a Docker repo AND are signed in
* use the provided script cld.sh <reponame> to build & push the repo and then install it on your Cloudron
* you will be prompted for Location: : enter where the app will be hosted, e.g. app.domain.tld
