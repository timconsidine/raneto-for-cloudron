#!/bin/bash

set -eu

# Define the setup indicator (e.g., a specific file or directory)
SETUP_INDICATOR="/app/data/.initialised"

# Check if the setup indicator exists
if [ ! -e "$SETUP_INDICATOR" ]; then
    # If the setup indicator does not exist, it means the app hasn't been set up
    echo "App not set up. Copying files..."

    mkdir -p /app/data/content /app/data/config /app/data/node_modules
    ln -s /app/data/node_modules/@raneto/theme-default/dist/public/images /app/data/images
    rm -rf /app/data/content/*
    cp -r /tmp/content/* /app/data/content
    cp /tmp/config.js /app/data/config/config.js
    rm -rf /app/data/node_modules/*
    cp -r /tmp/node_modules/* /app/data/node_modules

    # Create the setup indicator file to mark that the setup has been completed
    touch "$SETUP_INDICATOR"
    echo "initialised" > "$SETUP_INDICATOR"
else
    # If the setup indicator exists, the app has already been set up
    echo "App already set up. Skipping file copy."
fi

chown -R cloudron:cloudron /app/data

export npm_config_cache=/app/data/.npm-cache

exec /usr/local/bin/gosu cloudron:cloudron npm start
